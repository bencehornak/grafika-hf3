#include "World.h"

World::World(ShaderProgram& program) : Drawable(nullptr), program(program) {
}

World::~World() {
}

void World::onInitialize() {
    for (Drawable* obj : objects)
        obj->init();
}

void World::onDraw() {
    for (Drawable* obj : objects)
        obj->draw();
}
