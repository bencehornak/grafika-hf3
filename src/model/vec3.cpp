#define _USE_MATH_DEFINES
#include <cmath>

#include "vec3.h"

vec3::vec3() : x(0), y(0), z(0) {
}

vec3::vec3(GLfloat x, GLfloat y, GLfloat z)
: x(x), y(y), z(z) {
}

GLfloat vec3::norm() const {
    return sqrtf((*this) * (*this));
}

vec3 vec3::normalize() const {
    return (*this) / norm();
}

vec3 vec3::operator-() const {
    return vec3(-x, -y, -z);
}

vec3 vec3::operator+(const vec3& v) const {
    return vec3(x + v.x, y + v.y, z + v.z);
}

vec3 vec3::operator-(const vec3& v) const {
    return vec3(x - v.x, y - v.y, z - v.z);
}

vec3 vec3::operator*(GLfloat l) const {
    return vec3(l*x, l*y, l * z);
}

vec3 vec3::operator/(GLfloat l) const {
    return vec3(x / l, y / l, z / l);
}

GLfloat vec3::operator*(const vec3& v) const {
    return x * v.x + y * v.y + z * v.z;
}

vec3 vec3::cross(vec3& v) const {
    return vec3(y * v.z - z * v.y, -x * v.z + z * v.x, x * v.y - y * v.x);
}
