#ifndef TORUS_H
#define TORUS_H

#include "ParamSurface.h"


class Torus : public ParamSurface {
    GLfloat baseRadius, innerRadius;
public:
    Torus(Texture* texture, ParamSurfaceDrawer& drawer, GLfloat baseRadius, 
            GLfloat innerRadius);

    VertexData generateVertexData(GLfloat u, GLfloat v) override;
};

#endif /* TORUS_H */

