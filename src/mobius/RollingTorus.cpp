#include "RollingTorus.h"
#include "MobiusWorld.h"

const GLfloat RollingTorus::SPEED = .1;
const GLfloat RollingTorus::BASE_RADIUS = .4;
const GLfloat RollingTorus::INNER_RADIUS = .1;

RollingTorus::RollingTorus(MobiusWorld& world, Texture* texture, 
        const Material& material, bool left, GLfloat startingPhase) :
world(world), drawer(world, material, *this),
left(left), startingPhase(startingPhase), torus(texture, drawer, BASE_RADIUS, INNER_RADIUS) {
}

void RollingTorus::getTransformM(mat4& m, mat4& mInv) const {
    GLfloat phase = startingPhase + SPEED * world.time();
    world.getMobiusPosition(m, mInv, left ? 0.25 : 0.75, phase,
            BASE_RADIUS + INNER_RADIUS);
}
