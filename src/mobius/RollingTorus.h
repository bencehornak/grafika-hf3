#ifndef ROLLINGTORUS_H
#define ROLLINGTORUS_H

#include "model/Torus.h"
#include "model/mat4.h"
#include "RollingTorusDrawer.h"
#include "Material.h"

class MobiusWorld;

class RollingTorus {
protected:
    MobiusWorld& world;
private:
    RollingTorusDrawer drawer;

    const bool left;
    const GLfloat startingPhase;

    static const GLfloat SPEED;
    static const GLfloat BASE_RADIUS, INNER_RADIUS;
public:
    Torus torus;

    RollingTorus(MobiusWorld& world, Texture* texture, const Material& material,
            bool left, GLfloat startingPhase);

    void getTransformM(mat4& m, mat4& mInv) const;
};

#endif /* ROLLINGTORUS_H */

