#include "Drawer.h"
#include "MobiusWorld.h"


Drawer::Drawer(MobiusWorld& world, const Material& material)
: world(world), material(material) {
}

void Drawer::onInitialize() {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &vbo);
}

void Drawer::uploadTriangles(int numVertices, VertexData* triangleGrid) {

    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof (VertexData),
            triangleGrid, GL_STATIC_DRAW);

    // Enable the vertex attribute arrays
    glEnableVertexAttribArray(0); // attribute array 0 = POSITION
    glEnableVertexAttribArray(1); // attribute array 1 = NORMAL
    glEnableVertexAttribArray(2); // attribute array 2 = TEXCOORD0

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof (VertexData),
            (void*) offsetof(VertexData, position));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof (VertexData),
            (void*) offsetof(VertexData, normal));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof (VertexData),
            (void*) offsetof(VertexData, texcoord));

}

void Drawer::drawTriangles(int numVertices) {
    glBindVertexArray(vao);
    
    ShaderProgram& program = world.program;
    program.setUniform("kd", material.kd);
    program.setUniform("ks", material.ks);
    program.setUniform("ka", material.ka);
    program.setUniform("shine", material.shine);
    
    mat4 m, mInv;
    getTransformM(m, mInv);
    world.transformM(m, mInv);
    glDrawArrays(GL_TRIANGLES, 0, numVertices);
}

void Drawer::getTransformM(mat4& m, mat4& mInv) {
    m = mat4::identity();
    mInv = mat4::identity();
}
