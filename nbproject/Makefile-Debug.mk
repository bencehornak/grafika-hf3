#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=clang
CCC=clang++
CXX=clang++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=CLang-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/gl/Initializable.o \
	${OBJECTDIR}/src/gl/Shader.o \
	${OBJECTDIR}/src/gl/ShaderProgram.o \
	${OBJECTDIR}/src/gl/glsl_check.o \
	${OBJECTDIR}/src/mobius/CheckerBoardTexture.o \
	${OBJECTDIR}/src/mobius/Drawer.o \
	${OBJECTDIR}/src/mobius/Material.o \
	${OBJECTDIR}/src/mobius/Mobius.o \
	${OBJECTDIR}/src/mobius/MobiusProgram.o \
	${OBJECTDIR}/src/mobius/MobiusWorld.o \
	${OBJECTDIR}/src/mobius/RollingTorus.o \
	${OBJECTDIR}/src/mobius/RollingTorusDrawer.o \
	${OBJECTDIR}/src/mobius/SkyTexture.o \
	${OBJECTDIR}/src/mobius/controller.o \
	${OBJECTDIR}/src/mobius/main.o \
	${OBJECTDIR}/src/model/Camera.o \
	${OBJECTDIR}/src/model/Drawable.o \
	${OBJECTDIR}/src/model/Light.o \
	${OBJECTDIR}/src/model/ParamSurface.o \
	${OBJECTDIR}/src/model/Sphere.o \
	${OBJECTDIR}/src/model/Texture.o \
	${OBJECTDIR}/src/model/Torus.o \
	${OBJECTDIR}/src/model/World.o \
	${OBJECTDIR}/src/model/mat4.o \
	${OBJECTDIR}/src/model/vec2.o \
	${OBJECTDIR}/src/model/vec3.o \
	${OBJECTDIR}/src/model/vec4.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/hf3_mobius

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/hf3_mobius: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/hf3_mobius ${OBJECTFILES} ${LDLIBSOPTIONS} `pkg-config --libs freeglut glew`

${OBJECTDIR}/src/gl/Initializable.o: src/gl/Initializable.cpp
	${MKDIR} -p ${OBJECTDIR}/src/gl
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/gl/Initializable.o src/gl/Initializable.cpp

${OBJECTDIR}/src/gl/Shader.o: src/gl/Shader.cpp
	${MKDIR} -p ${OBJECTDIR}/src/gl
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/gl/Shader.o src/gl/Shader.cpp

${OBJECTDIR}/src/gl/ShaderProgram.o: src/gl/ShaderProgram.cpp
	${MKDIR} -p ${OBJECTDIR}/src/gl
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/gl/ShaderProgram.o src/gl/ShaderProgram.cpp

${OBJECTDIR}/src/gl/glsl_check.o: src/gl/glsl_check.cpp
	${MKDIR} -p ${OBJECTDIR}/src/gl
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/gl/glsl_check.o src/gl/glsl_check.cpp

${OBJECTDIR}/src/mobius/CheckerBoardTexture.o: src/mobius/CheckerBoardTexture.cpp
	${MKDIR} -p ${OBJECTDIR}/src/mobius
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/mobius/CheckerBoardTexture.o src/mobius/CheckerBoardTexture.cpp

${OBJECTDIR}/src/mobius/Drawer.o: src/mobius/Drawer.cpp
	${MKDIR} -p ${OBJECTDIR}/src/mobius
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/mobius/Drawer.o src/mobius/Drawer.cpp

${OBJECTDIR}/src/mobius/Material.o: src/mobius/Material.cpp
	${MKDIR} -p ${OBJECTDIR}/src/mobius
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/mobius/Material.o src/mobius/Material.cpp

${OBJECTDIR}/src/mobius/Mobius.o: src/mobius/Mobius.cpp
	${MKDIR} -p ${OBJECTDIR}/src/mobius
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/mobius/Mobius.o src/mobius/Mobius.cpp

${OBJECTDIR}/src/mobius/MobiusProgram.o: src/mobius/MobiusProgram.cpp
	${MKDIR} -p ${OBJECTDIR}/src/mobius
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/mobius/MobiusProgram.o src/mobius/MobiusProgram.cpp

${OBJECTDIR}/src/mobius/MobiusWorld.o: src/mobius/MobiusWorld.cpp
	${MKDIR} -p ${OBJECTDIR}/src/mobius
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/mobius/MobiusWorld.o src/mobius/MobiusWorld.cpp

${OBJECTDIR}/src/mobius/RollingTorus.o: src/mobius/RollingTorus.cpp
	${MKDIR} -p ${OBJECTDIR}/src/mobius
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/mobius/RollingTorus.o src/mobius/RollingTorus.cpp

${OBJECTDIR}/src/mobius/RollingTorusDrawer.o: src/mobius/RollingTorusDrawer.cpp
	${MKDIR} -p ${OBJECTDIR}/src/mobius
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/mobius/RollingTorusDrawer.o src/mobius/RollingTorusDrawer.cpp

${OBJECTDIR}/src/mobius/SkyTexture.o: src/mobius/SkyTexture.cpp
	${MKDIR} -p ${OBJECTDIR}/src/mobius
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/mobius/SkyTexture.o src/mobius/SkyTexture.cpp

${OBJECTDIR}/src/mobius/controller.o: src/mobius/controller.cpp
	${MKDIR} -p ${OBJECTDIR}/src/mobius
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/mobius/controller.o src/mobius/controller.cpp

${OBJECTDIR}/src/mobius/main.o: src/mobius/main.cpp
	${MKDIR} -p ${OBJECTDIR}/src/mobius
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/mobius/main.o src/mobius/main.cpp

${OBJECTDIR}/src/model/Camera.o: src/model/Camera.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/Camera.o src/model/Camera.cpp

${OBJECTDIR}/src/model/Drawable.o: src/model/Drawable.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/Drawable.o src/model/Drawable.cpp

${OBJECTDIR}/src/model/Light.o: src/model/Light.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/Light.o src/model/Light.cpp

${OBJECTDIR}/src/model/ParamSurface.o: src/model/ParamSurface.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/ParamSurface.o src/model/ParamSurface.cpp

${OBJECTDIR}/src/model/Sphere.o: src/model/Sphere.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/Sphere.o src/model/Sphere.cpp

${OBJECTDIR}/src/model/Texture.o: src/model/Texture.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/Texture.o src/model/Texture.cpp

${OBJECTDIR}/src/model/Torus.o: src/model/Torus.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/Torus.o src/model/Torus.cpp

${OBJECTDIR}/src/model/World.o: src/model/World.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/World.o src/model/World.cpp

${OBJECTDIR}/src/model/mat4.o: src/model/mat4.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/mat4.o src/model/mat4.cpp

${OBJECTDIR}/src/model/vec2.o: src/model/vec2.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/vec2.o src/model/vec2.cpp

${OBJECTDIR}/src/model/vec3.o: src/model/vec3.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/vec3.o src/model/vec3.cpp

${OBJECTDIR}/src/model/vec4.o: src/model/vec4.cpp
	${MKDIR} -p ${OBJECTDIR}/src/model
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -Isrc -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/model/vec4.o src/model/vec4.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
